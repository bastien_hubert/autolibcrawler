package com.bhubert;

import javax.ws.rs.*;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Path(value = "/autolib")
@Produces("application/json")
public class AutolibCrawler {

    @GET
    @Path("/allStations")
    public String getAllStations() throws IOException {
        return getAllStationsJson();

    }

    @GET
    @Path("/station/")
    public String getStation(@QueryParam("stationId") List<String> stationIds) throws IOException {
        String stations = "";
        for (String stationId : stationIds) {

            String allStationsJson = getAllStationsJson();

            Pattern pattern = Pattern.compile("(([a-zA-Z0-9\"_,:\\.\\s\\{])*" + stationId + "([a-zA-Z0-9\"_,:\\.\\s\\}])*)");
            Matcher m = pattern.matcher(allStationsJson);
            if (m.find()) {

                stations += m.group(0).replace(", {", "{").replace("}, ", "},");
            }
        }

        return stations.replace("},", "}");


    }


    private String getAllStationsJson() throws IOException {
        URL url = new URL("https://www.autolib.eu/stations");
        BufferedReader in = new BufferedReader(
                new InputStreamReader(url.openStream()));

        String totalPage = "";
        String inputLine;
        while ((inputLine = in.readLine()) != null)
            totalPage += inputLine;
        in.close();


        Pattern pattern = Pattern.compile(".*((initMap).*;).*");
        Matcher m = pattern.matcher(totalPage);
        m.matches();

        return "["+m.group(1).replace("initMap([", "").split("\\);")[0].split("\\], \\{")[0]+"]";
    }
}
