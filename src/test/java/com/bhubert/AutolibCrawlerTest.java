package com.bhubert;

import org.junit.Test;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static org.junit.Assert.*;

/**
 * Created by bastien on 16/12/15.
 */
public class AutolibCrawlerTest {

    @Test
    public void getStationTest() throws IOException {
        List<String> params = new ArrayList<>();
        params.add("Paris/Voltaire/106");

        AutolibCrawler autolibCrawler = new AutolibCrawler();
        System.out.println(autolibCrawler.getStation(params));
    }

    @Test
    public void getAllStations() throws IOException {
        AutolibCrawler autolibCrawler = new AutolibCrawler();
        System.out.println(autolibCrawler.getAllStations());

    }

    @Test
    public void test() throws IOException {
        String lol = "tototo pwet lol pwet lolilol pwet sarace";
        Pattern pattern = Pattern.compile("\\spwet\\s");

        Matcher matcher = pattern.matcher(lol);
        while(matcher.find()) {
            System.out.println(matcher.group());
        }

    }

}